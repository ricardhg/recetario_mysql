import React, { useState } from "react";

import { Link, BrowserRouter, Switch, Route } from 'react-router-dom';

import ListaProductos from './ListaProductos';
import ListaRecetas from './ListaRecetas';
import EditaReceta from './EditaReceta';
import ModificaReceta from './ModificaReceta';
import Inicio from './Inicio';
import Error404 from './Error404';

import {
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';

const Navegacion = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="light" light expand="md">
        <Link className="navbar-brand" to="/">Recetario</Link>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>

            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Páginas
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  <Link to="/ListaProductos">Productos</Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/ListaRecetas">Recetas</Link>
                </DropdownItem>

              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>

        </Collapse>
      </Navbar>
    </div>
  );
}






export default function App() {

  return (
    <BrowserRouter>

      <Navegacion />
      <Container>
        <br />

        <Switch>
          <Route exact path="/" component={Inicio} />
          <Route path="/ListaProductos" component={ListaProductos} />
          <Route path="/ListaRecetas" component={ListaRecetas} />
          <Route path="/EditaReceta/:idReceta" component={EditaReceta} />
          <Route path="/ModificaReceta/:idReceta" component={ModificaReceta} />
          <Route component={Error404} />
        </Switch>

      </Container>
    </BrowserRouter>
  )
}