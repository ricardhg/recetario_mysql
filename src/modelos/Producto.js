

const APIURL = 'http://localhost:3000/api/';
const MODEL = 'productos';

class Producto {

    static getProductos = () => {
        let url = APIURL + MODEL;
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    };

    static getProductoById = (idSolicitado) => {
        let url = APIURL + MODEL + "/" + idSolicitado;
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    }


    static addProducto = (nombreProducto, callback) => {
        let datos = {
            nombre: nombreProducto
        }
        let url = APIURL + MODEL;

        fetch(url, {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(datos)
        })
        .then(() => callback())
        .catch(err => console.log(err));
    }


    static eliminaProducto = (idBorrar, callback) => {
        let url = APIURL + MODEL + "/" + idBorrar;
        fetch(url, { method: 'DELETE' })
            .then(() => callback())
            .catch(err => console.log(err));
    }



}

export default Producto;