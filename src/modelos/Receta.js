
const APIURL = 'http://localhost:3000/api/';
const MODEL = 'recetas';


class Receta {

    static getRecetas = () => {
        let url = APIURL + MODEL;
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    };
  

    static getRecetaById = (idSolicitado) => {
        let url = APIURL + MODEL + "/" + idSolicitado;
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data[0]))
                .catch(err => reject(err));
        });
    }


    static addReceta = (nombre, descripcion, callback) => {
        let datos = {
            nombre,
            descripcion
        }
        let url = APIURL + MODEL;

        fetch(url, {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(datos)
        })
        .then(() => callback())
        .catch(err => console.log(err));
    }

    

    static modificaReceta = (id, nombre, descripcion, callback) => {
        let datos = {
            id,
            nombre,
            descripcion
        }
        let url = APIURL + MODEL;

        fetch(url, {
            method: 'PUT',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(datos)
        })
        .then(() => callback())
        .catch(err => console.log(err));
    }


    
    static eliminaReceta = (idBorrar, callback) => {
        let url = APIURL + MODEL + "/" + idBorrar;
        fetch(url, { method: 'DELETE' })
            .then(() => callback())
            .catch(err => console.log(err));
    }



}

export default Receta;