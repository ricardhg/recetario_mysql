
const APIURL = 'http://localhost:3000/api/';
const MODEL = 'ingredientes';
class Ingrediente {


    static getIngredientes = (idRecetaPedida) => {
        let url = APIURL + MODEL;
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(todos => todos.filter(ing => ing.recetas_id===idRecetaPedida*1))
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    };

    static getTodosIngredientes = () => {
        let url = APIURL + MODEL;
        return new Promise((resolve, reject) => {
            fetch(url, { method: 'GET' })
                .then(results => results.json())
                .then(data => resolve(data))
                .catch(err => reject(err));
        });
    };

    static addIngrediente = ({idReceta, idProducto, unidades, cantidad}, callback) => {
        let datos = {
            recetas_id: idReceta,
            productos_id: idProducto,
            unidades,
            cantidad
        }

        console.log("ingredeinte nuevl", datos)
        let url = APIURL + MODEL;

        fetch(url, {
            method: 'POST',
            headers: new Headers({ 'Content-Type': 'application/json' }),
            body: JSON.stringify(datos)
        })
        .then(() => callback())
        .catch(err => console.log(err));
    }
 
   
    static eliminaIngrediente = (idBorrar, callback) => {
        let url = APIURL + MODEL + "/" + idBorrar;
        fetch(url, { method: 'DELETE' })
            .then(() => callback())
            .catch(err => console.log(err));
    }


}

export default Ingrediente;
