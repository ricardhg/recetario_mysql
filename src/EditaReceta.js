import React from 'react';
import Receta from './modelos/Receta';
import Producto from './modelos/Producto';
import Ingrediente from './modelos/Ingrediente';
import { Link } from 'react-router-dom';
import { Input, Row, Col, Table, Button } from 'reactstrap';


const UNIDADES = [
    'Unidades',
    'Vasos',
    'Tazas',
    'Cucharadas',
    'Litros',
    'ml',
    'Kg',
    'gramos'
];

export default class EditaReceta extends React.Component {

    constructor(props) {
        super(props);

      
        //this.setState(receta);
        this.state = {
            id: 0,
            nombre: '',
            descripcion: '',
            ingredientes : [],
            productos: [],
            idIngrediente: '-1',
            unidades: UNIDADES[0],
            cantidad: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.ingredienteNuevo = this.ingredienteNuevo.bind(this);
        this.borrarIngrediente = this.borrarIngrediente.bind(this);

        this.cargaDatos = this.cargaDatos.bind(this);
        this.actualizaIngredientes = this.actualizaIngredientes.bind(this);

        this.cargaDatos();
        this.actualizaIngredientes();

    }

    cargaDatos(){
        Producto.getProductos()
        .then(data => {
            this.setState({productos: data});
        })
        .catch(err => console.log(err));

        Receta.getRecetaById(this.props.match.params.idReceta * 1)
        .then(receta => {
            console.log("receta recibida:", receta)
            this.setState({
                id: receta.id,
                nombre: receta.nombre,
                descripcion: receta.descripcion});
        })
        .catch(err => console.log(err));
    }

    actualizaIngredientes(){
        Ingrediente.getIngredientes(this.props.match.params.idReceta * 1)
        .then(data => {
            this.setState({ingredientes: data});
        })
        .catch(err => console.log(err));
    }

    handleInputChange(evento) {
        const target = evento.target;
        const value = (target.type === 'checkbox') ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    ingredienteNuevo(){
        let idReceta=this.state.id;
        let idProducto=this.state.idIngrediente;
        let unidades = this.state.unidades;
        let cantidad = this.state.cantidad;

        let nuevoIngrediente = {
            idReceta: idReceta, 
            idProducto: idProducto, 
            unidades, 
            cantidad
        };

        Ingrediente.addIngrediente(nuevoIngrediente, this.actualizaIngredientes);
    }

    borrarIngrediente(id){
        Ingrediente.eliminaIngrediente(id, this.actualizaIngredientes);
    }

//  <td>{Producto.getProductoById(ing.idProducto*1)['nombre']}</td> 
// faria una consula a bdd cada vegada
                
    render() {
        let opcionesProducto = this.state.productos.map(el => <option key={el.id} value={el.id}>{el.nombre}</option>);
     
        let unidades = UNIDADES.map((el, index) => <option key={index} value={el}>{el}</option>);
     
        let filas = this.state.ingredientes.map(ing => 
                <tr key={ing.id}>
                   <td>{ing.id}</td> 
                   <td>{this.state.productos.find(el => el.id===ing.productos_id*1)['nombre']}</td> 
                   <td>{ing.cantidad}</td> 
                   <td>{ing.unidades}</td> 
                   <td><Button onClick={()=>this.borrarIngrediente(ing.id)} >Borrar</Button></td> 
                </tr>
            );


        return (
            <>

                <Row>
                    <Col>
                        <h2>Editando receta: {this.state.nombre}</h2>
                        <br />
                    </Col>
                </Row>
                <Row>
                    <Col xs="4">
                        <h5>Nuevo ingrediente:</h5>
                        <Input className="mb-2" type="select" name="idIngrediente" onChange={this.handleInputChange} value={this.state.idIngrediente} >
                            <option value='-1'>---</option>
                            {opcionesProducto}
                        </Input>

                        <Input className="mb-2" type="text" name="cantidad" onChange={this.handleInputChange} value={this.state.cantidad}/>
                       
                        <Input className="mb-2" type="select"
                                name="unidades" 
                                onChange={this.handleInputChange} 
                                value={this.state.unidades} >
                            
                            {unidades}
                          
                        </Input>
                    
                        <Button disabled={this.state.idIngrediente==="-1"} onClick={this.ingredienteNuevo}>Añadir</Button>

                        <br />
                        <br />
                        <Link to="/ListaRecetas">Volver al listado</Link>

                    </Col>
                    <Col >
                        <h5>Descripción:</h5>
                        <p>{this.state.descripcion}</p>
                        <Table>
                            <thead>
                                <tr><th>id</th><th>Ingrediente</th><td>Qtt</td><td>Ud</td><td></td></tr>
                            </thead>
                            <tbody>
                                {filas}
                            </tbody>
                        </Table>
                    </Col>
                </Row>



            </>
        )
    }

}